package com.hashcode.rides;

public class Ride {
	
	private int id;
	private int earlyStart;
	private int latestFinish;
	private int[] startingCoords;
	private int[] finishCoords;
	private int bonusPoints;
	private boolean isAssigned;
	
	public Ride(int id, int earlyStart, int latestFinish, int[] startingCoords, int[] finishCoords, int bonusPoints, boolean isAssigned) {
		this.setId(id);
		this.earlyStart = earlyStart;
		this.setLatestFinish(latestFinish);
		this.startingCoords = startingCoords;
		this.finishCoords = finishCoords;
		this.bonusPoints = bonusPoints;
		this.isAssigned = isAssigned;
	}

	public int getEarlyStart() {
		return earlyStart;
	}

	public void setEarlyStart(int earlyStart) {
		this.earlyStart = earlyStart;
	}

	public int getLatestFinish() {
		return latestFinish;
	}

	public void setLatestFinish(int latestFinish) {
		this.latestFinish = latestFinish;
	}

	public int[] getStartingCoords() {
		return startingCoords;
	}

	public void setStartingCoords(int[] startingCoords) {
		this.startingCoords = startingCoords;
	}

	public int[] getFinishCoords() {
		return finishCoords;
	}

	public void setFinishCoords(int[] finishCoords) {
		this.finishCoords = finishCoords;
	}

	public int getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	public boolean isAssigned() {
		return isAssigned;
	}

	public void setAssigned(boolean isAssigned) {
		this.isAssigned = isAssigned;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
