package com.hashcode.rides;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.LineIterator;

public class Reader {

	public static FileContent readFile(String fileName) {
		FileContent res = new FileContent();
		String normalized = FilenameUtils.normalize(fileName);
		File file = new File(normalized);
		try {
			LineIterator it = FileUtils.lineIterator(file);
			String firstLine = it.hasNext() ? it.nextLine() : "";
			String[] gridAttrs = firstLine.split(" ");
			if (gridAttrs.length != 6) {
				throw new RuntimeException("Wrong format on grid definition line.");
			}
			res.setRows(Integer.parseInt(gridAttrs[0]));
			res.setColumns(Integer.parseInt(gridAttrs[1]));
			res.setVehicleNumber(Integer.parseInt(gridAttrs[2]));
			res.setRideNumber(Integer.parseInt(gridAttrs[3]));
			res.setBonus(Integer.parseInt(gridAttrs[4]));
			res.setMaxSteps(Integer.parseInt(gridAttrs[5]));
			res.setRides(new ArrayList<Ride>());
			Ride currRide;
			int currLine = 0;
			while (it.hasNext()) {
				String line = it.nextLine();
				String[] rideAttrs = line.split(" ");
				if (rideAttrs.length != 6) {
					throw new RuntimeException("Wrong format on ride definition line "+(res.getRides().size()+1));
				}
				int[] startCoords = {Integer.parseInt(rideAttrs[0]),Integer.parseInt(rideAttrs[1])};
				int[] endCoords = {Integer.parseInt(rideAttrs[2]),Integer.parseInt(rideAttrs[3])};
				currRide = new Ride(currLine, Integer.parseInt(rideAttrs[4]),Integer.parseInt(rideAttrs[5]),startCoords,endCoords,0,false);
				res.getRides().add(currRide);
				currLine++;
			}
			if (res.getRides().size() != res.getRideNumber()) {
				throw new RuntimeException("Wrong number of rides in the file.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

}
