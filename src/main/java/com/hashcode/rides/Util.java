package com.hashcode.rides;

public class Util {

	public static int getDistance(int[] startingCoords, int[] finishCoords) {
        int journeyDistance= Math.abs(finishCoords[0] - startingCoords[0]) + 
                    Math.abs(finishCoords[1] - startingCoords[1]);
        return journeyDistance;
	}
}
