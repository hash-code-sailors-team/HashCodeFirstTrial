package com.hashcode.rides;

public class Travel {
	
	private int rideId;
	private int[] whereIsFree;
	private int whenIsFree;
	
	public Travel(int rideId, int[] whereIsFree, int whenIsFree) {
		this.rideId = rideId;
		this.whereIsFree = whereIsFree;
		this.whenIsFree = whenIsFree;
	}
	
	public int getRideId() {
		return rideId;
	}
	public void setRideId(int rideId) {
		this.rideId = rideId;
	}
	public int[] getWhereIsFree() {
		return whereIsFree;
	}
	public void setWhereIsFree(int[] whereIsFree) {
		this.whereIsFree = whereIsFree;
	}
	public int getWhenIsFree() {
		return whenIsFree;
	}
	public void setWhenIsFree(int whenIsFree) {
		this.whenIsFree = whenIsFree;
	}

}
