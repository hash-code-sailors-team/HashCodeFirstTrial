package com.hashcode.rides;

import java.util.ArrayList;
import java.util.List;

public class Car {
	
	int id;
	private List<Travel> travels;
	private int[] routes;
	
	int currentX;
	int currentY;
	int currentStep;

	public Car(int id, List<Travel> travels) {
		this.id = id;
		this.travels = travels;
	}
	
	public Car(int id) {
		this.id = id;
		this.travels = new ArrayList<Travel>();
		currentX = 0;
		currentY = 0;
		currentStep = 0;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public List<Travel> getTravels() {
		return travels;
	}

	public void setTravels(List<Travel> travels) {
		this.travels = travels;
	}
	
	public void bestRoute(int[] currentPosition, int[] nextStartRide, int distanceNextRide, int currentTime, 
			int finalTime, int bonus, int startTime, int upperLimit, int intRightLimit) {
		
		routes = new int[]{-1000, -1000, -1000, -1000};
		
		if (currentPosition[1] != upperLimit - 1) {
			int wUp = getWeight(new int[]{currentPosition[0]+1, currentPosition[1]}, nextStartRide, distanceNextRide, bonus, startTime, currentTime, finalTime);
			routes[0] = wUp;
		}
		
		if (currentPosition[1] != 0) {
			int wDown = getWeight(new int[]{currentPosition[0]+1, currentPosition[1]}, nextStartRide, distanceNextRide, bonus, startTime, currentTime, finalTime);
			routes[1] = wDown;
		}
		
		if (currentPosition[0] != intRightLimit - 1) {
			int wRight = getWeight(new int[]{currentPosition[0]+1, currentPosition[1]}, nextStartRide, distanceNextRide, bonus, startTime, currentTime, finalTime);
			routes[2] = wRight;
		}
		
		if (currentPosition[0] != 0) {
			int wLeft = getWeight(new int[]{currentPosition[0]-1, currentPosition[1]}, nextStartRide, distanceNextRide, bonus, startTime, currentTime, finalTime);
			routes[3] = wLeft;
		}

	}
	
	private int getWeight(int[] currentPosition, int[] nextStartRide, int distanceNextRide, int bonus, int startTime, int currentTime, 
			int finalTime) {
		int distance = getDistance(currentPosition, nextStartRide);
		if (finalTime - currentTime - distance > 0) {
			return (distanceNextRide + bonus - getDistance(currentPosition, nextStartRide) - startTime - distance);
		} else {
			return -1000;
		}
	}
	
	private int getDistance(int[] currentPosition, int[] nextStartRide) {
		return Math.abs(currentPosition[0]-nextStartRide[0]) + Math.abs(currentPosition[1]-nextStartRide[1]);
	}

	public int[] getRoutes() {
		return routes;
	}

	public void setRoutes(int[] routes) {
		this.routes = routes;
	}

	public int getCurrentX() {
		return currentX;
	}

	public void setCurrentX(int currentX) {
		this.currentX = currentX;
	}

	public int getCurrentY() {
		return currentY;
	}

	public void setCurrentY(int currentY) {
		this.currentY = currentY;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}
	
}
