package com.hashcode.rides;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ResultWriter {

	public static void writeResults(String fileName, FileResult result) {
		File outFile = new File(fileName);
		try (FileWriter writer = new FileWriter(outFile)) {
			for (Car c : result.getCarList()) {
				StringBuffer resString = new StringBuffer(c.getTravels().size()+2);
				resString.append(c.getTravels().size());
				for (Travel t : c.getTravels()) {
					resString.append(" " + t.getRideId());
				}
				try {
					writer.append(resString.toString()+'\n');
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
