package com.hashcode.rides;

import java.util.List;

public interface RideDistributionAlgorithm {
	
	public FileResult distributeCars(List<Car> carPool, List<Ride> rideList, int maxRows, int maxColumns, 
			int globalBonus, int maxSteps, 
			double rateGlobalBonus, double rateDistanceToStart, double rateJourneyDistance, double rateCarWaitTime,
			double rateNextBonus, boolean isMetropolis);

}
