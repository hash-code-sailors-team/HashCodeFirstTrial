package com.hashcode.rides;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.hashcode.rides.algorithms.NaiveApproach;

public class App {
	
	public static int calcDist(String fileName, double rateGlobalBonus, double rateDistanceToStart, double rateJourneyDistance, double rateCarWaitTime,
			double rateNextBonus, boolean isMetropolis) {
		System.out.println("rateGlobalBonus : " + rateGlobalBonus + 
				", rateDistanceToStart : " + rateDistanceToStart + 
				", rateJourneyDistance : " + rateJourneyDistance + 
				", rateCarWaitTime : " + rateCarWaitTime + 
				", rateNextBonus : " + rateNextBonus);
		long startTime = System.currentTimeMillis();
		FileContent fc = Reader.readFile(System.getProperty("user.dir") + "/"+fileName+".in");
		List<Car> carPool = new ArrayList<>();
		for (int i = 0; i < fc.getVehicleNumber(); i++) {
			carPool.add(new Car(i, new ArrayList<Travel>()));
		}
		List<Ride> rideList = fc.getRides();
		int globalBonus = fc.getBonus();
		int maxRow = fc.getRows() - 1;
		int maxColumn = fc.getColumns() - 1;
		int maxSteps = fc.getMaxSteps();

		/*System.out.println("The city goes from 0,0 to " + maxRow + "," + maxColumn);
		System.out.println("The bonus for matching the earlier start is " + globalBonus);
		System.out.println("There are " + carPool.size() + " cars in the pool");
		System.out.println("There are " + rideList.size() + " people ordering rides");
		System.out.println("The world ends in " + fc.getMaxSteps() + " steps");*/
		
		RideDistributionAlgorithm algorithm = new NaiveApproach();
		FileResult result = algorithm.distributeCars(carPool, rideList, maxRow, maxColumn, globalBonus, maxSteps,
				rateGlobalBonus, rateDistanceToStart, rateJourneyDistance, rateCarWaitTime, rateNextBonus, isMetropolis);
		System.out.println("Reward for file " + fileName + " : " + result.getTotalReward());
		String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		ResultWriter.writeResults(System.getProperty("user.dir") + "/"+fileName + timeLog+".out", result);
		long endTime = System.currentTimeMillis();
		System.out.println("Total time : " + ((endTime - startTime)/1000));
		return result.getTotalReward();
		
	}
	
	public static void main(String[] args) {
		int totalReward = 0;
		totalReward += calcDist("a_example", 1, 1, 1, 1, 0, false); //10
		totalReward += calcDist("b_should_be_easy", 1, 0.9, 0.8, 0.1, 0, false); //175177 
		totalReward += calcDist("c_no_hurry", 1, 1, 0.5, 1, 0, false); // 15477994
		totalReward += calcDist("d_metropolis", 1.2, 1, -0.3, 1, 0, true); //11736652
		totalReward += calcDist("e_high_bonus", 1.3, 1, 1, 1, 0, false); //20743975 */
		System.out.println("TOTAL REWARD : " + totalReward);
	}
	
}
