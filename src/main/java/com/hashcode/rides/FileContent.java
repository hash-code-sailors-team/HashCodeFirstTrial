package com.hashcode.rides;

import java.util.List;

public class FileContent {
	
	private Integer rows;
	private Integer columns;
	
	private Integer vehicleNumber;
	
	private Integer rideNumber;
	private List<Ride> rides;
	
	private Integer bonus;
	
	private Integer maxSteps;

	/**
	 * @return the rows
	 */
	public Integer getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(Integer rows) {
		this.rows = rows;
	}

	/**
	 * @return the columns
	 */
	public Integer getColumns() {
		return columns;
	}

	/**
	 * @param columns the columns to set
	 */
	public void setColumns(Integer columns) {
		this.columns = columns;
	}

	/**
	 * @return the vehicleNumber
	 */
	public Integer getVehicleNumber() {
		return vehicleNumber;
	}

	/**
	 * @param vehicleNumber the vehicleNumber to set
	 */
	public void setVehicleNumber(Integer vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	/**
	 * @return the rideNumber
	 */
	public Integer getRideNumber() {
		return rideNumber;
	}

	/**
	 * @param rideNumber the rideNumber to set
	 */
	public void setRideNumber(Integer rideNumber) {
		this.rideNumber = rideNumber;
	}

	/**
	 * @return the rides
	 */
	public List<Ride> getRides() {
		return rides;
	}

	/**
	 * @param rides the rides to set
	 */
	public void setRides(List<Ride> rides) {
		this.rides = rides;
	}

	/**
	 * @return the bonus
	 */
	public Integer getBonus() {
		return bonus;
	}

	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}

	/**
	 * @return the maxSteps
	 */
	public Integer getMaxSteps() {
		return maxSteps;
	}

	/**
	 * @param maxSteps the maxSteps to set
	 */
	public void setMaxSteps(Integer maxSteps) {
		this.maxSteps = maxSteps;
	}

}
