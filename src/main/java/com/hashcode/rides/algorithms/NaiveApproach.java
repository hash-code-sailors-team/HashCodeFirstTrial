package com.hashcode.rides.algorithms;

import java.util.List;

import com.hashcode.rides.Car;
import com.hashcode.rides.FileResult;
import com.hashcode.rides.Ride;
import com.hashcode.rides.RideDistributionAlgorithm;
import com.hashcode.rides.Travel;

public class NaiveApproach implements RideDistributionAlgorithm {

	@Override
	public FileResult distributeCars(List<Car> carPool, List<Ride> rideList, int maxRows, int maxColumns,
			int globalBonus, int maxSteps, 
			double rateGlobalBonus, double rateDistanceToStart, double rateJourneyDistance, double rateCarWaitTime,
			double rateNextBonus, boolean isMetropolis) {
		FileResult res = new FileResult();
		int totalReward = 0;
		boolean isRideFound = true;

		while(isRideFound) {
			double maxCredit = 0;
			int selectedCar = -1;
			int selectedRide = -1;
			isRideFound = false;
			for (Car car : carPool) {
				for(Ride ride : rideList) {
					// We iterate what the most rewarding pair of ride / car
					if (!ride.isAssigned()) {
						int[] startingCoords = ride.getStartingCoords();
						int[] finishCoords = ride.getFinishCoords();
						int distanceToStart = Math.abs(car.getCurrentX() - startingCoords[0])
								+ Math.abs(car.getCurrentY() - startingCoords[1]);
						int carWaitTime = ride.getEarlyStart() - car.getCurrentStep() - distanceToStart;
						carWaitTime = carWaitTime < 0 ? 0 : carWaitTime;
						int rideWaitTime = car.getCurrentStep() + distanceToStart - ride.getEarlyStart();
						rideWaitTime = rideWaitTime < 0 ? 0 : rideWaitTime;
						
						int journeyDistance = Math.abs(finishCoords[0] - startingCoords[0])
								+ Math.abs(finishCoords[1] - startingCoords[1]);
						double currentRateJourneyDistance = 0;
						if(isMetropolis) {
							currentRateJourneyDistance = finishCoords[0]<500 || finishCoords[0]>4000 || finishCoords[1]>4000 ? rateJourneyDistance : 1;
						} else {
							currentRateJourneyDistance = rateJourneyDistance;
						}
						double currentBonus = 
								rateGlobalBonus * (rideWaitTime==0?globalBonus:0) - 
								rateDistanceToStart * distanceToStart + 
								currentRateJourneyDistance * journeyDistance - 
								rateCarWaitTime * carWaitTime;
						// Search for the next ride
						// within 1 cell and starting next turn at most
						Ride selectedNextRide = null;
						double maxNextBonus = 0;
						if(rateNextBonus!=0) {
							for(Ride nextRide : rideList) {
								/*int nextDistance = Math.abs(finishCoords[0] - nextRide.getStartingCoords()[0])
										+ Math.abs(finishCoords[1] - nextRide.getStartingCoords()[1]);*/
								int finishCurrent = car.getCurrentStep() + distanceToStart + carWaitTime + journeyDistance;
								//if((nextDistance < 2) && (nextRide.getEarlyStart() - finishCurrent) < 2) {
									
									int[] startingCoordsNext = nextRide.getStartingCoords();
									int[] finishCoordsNext = nextRide.getFinishCoords();
									int distanceToStartNext = Math.abs(finishCoords[0] - nextRide.getFinishCoords()[0])
											+ Math.abs(finishCoords[1] - nextRide.getFinishCoords()[1]);
									int carWaitTimeNext = nextRide.getEarlyStart() - finishCurrent - distanceToStartNext;
									carWaitTimeNext = carWaitTimeNext < 0 ? 0 : carWaitTimeNext;
									int rideWaitTimeNext = finishCurrent + distanceToStartNext - nextRide.getEarlyStart();
									rideWaitTimeNext = rideWaitTimeNext < 0 ? 0 : rideWaitTimeNext;
									
									int journeyDistanceNext = Math.abs(finishCoordsNext[0] - startingCoordsNext[0])
											+ Math.abs(finishCoordsNext[1] - startingCoordsNext[1]);
									
									double nextBonus = rateGlobalBonus * (rideWaitTimeNext==0?globalBonus:0) - 
									rateDistanceToStart * distanceToStartNext + 
									rateJourneyDistance * journeyDistanceNext - 
									rateCarWaitTime * carWaitTimeNext;
									if(selectedNextRide==null || nextBonus > maxNextBonus) {
										selectedNextRide = nextRide;
									}
								//}
							}
						}
						currentBonus += rateNextBonus * maxNextBonus;
						if ((!isRideFound || currentBonus > maxCredit) && 
								ride.getLatestFinish() > car.getCurrentStep() + distanceToStart + carWaitTime + journeyDistance) {
							selectedCar = car.getId();
							selectedRide = ride.getId();
							isRideFound = true;
							maxCredit = currentBonus;
						}
					}
					//currentPos++;
				}
			}
			if (isRideFound) {
				Ride ride = rideList.get(selectedRide);
				Car car = carPool.get(selectedCar);
				ride.setAssigned(true);
				car.getTravels().add(new Travel(ride.getId(), null, 0));
				int distanceToStart = Math.abs(car.getCurrentX() - ride.getStartingCoords()[0])
						+ Math.abs(car.getCurrentY() - ride.getStartingCoords()[1]);
				int carWaitTime = ride.getEarlyStart() - car.getCurrentStep() - distanceToStart;
				carWaitTime = carWaitTime < 0 ? 0 : carWaitTime;
				int rideWaitTime = car.getCurrentStep() + distanceToStart - ride.getEarlyStart();
				rideWaitTime = rideWaitTime < 0 ? 0 : rideWaitTime;
				int[] startingCoords = ride.getStartingCoords();
				int[] finishCoords = ride.getFinishCoords();
				int journeyDistance = Math.abs(finishCoords[0] - startingCoords[0])
						+ Math.abs(finishCoords[1] - startingCoords[1]);
				car.setCurrentStep(car.getCurrentStep() + distanceToStart + carWaitTime + journeyDistance);
				// TO DO : This needs to be fixed. Not sure if totalReward is right
				totalReward += journeyDistance + (rideWaitTime==0?globalBonus:0);
				car.setCurrentX(ride.getFinishCoords()[0]);
				car.setCurrentY(ride.getFinishCoords()[1]);
			}
		}
		res.setCarList(carPool);
		res.setTotalReward(totalReward);
		return res;
	}

}
